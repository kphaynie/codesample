﻿using BasicRegister;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BasicRegisterTests
{
    [TestClass]
    public class TestBuyable
    {
        Buyable testItemOne = new Buyable("Test Item One", 12345, 14.00m, true, false);
        Buyable testItemTwo = new Buyable("Test Item Two", 23456, 10.01m, true, true);
        Buyable testItemDuplicate = new Buyable("Test Item Duplicate", 12345, 111.00m, true, false);

        [TestMethod]
        public void TestEquality()
        {
            Assert.AreEqual(testItemOne, testItemDuplicate);
            Assert.AreNotEqual(testItemOne, testItemTwo);
        }

        [TestMethod]
        public void TestToString()
        {
            string expectedOne = "Test Item One";
            string expectedTwo = "Imported Test Item Two";

            Assert.AreEqual(expectedOne, testItemOne.ToString());
            Assert.AreEqual(expectedTwo, testItemTwo.ToString());
        }
    }

    [TestClass]
    public class TestCatalog
    {
        Buyable testItemOne = new Buyable("Test Item One", 12345, 14.00m, true, false);
        Buyable testItemTwo = new Buyable("Test Item Two", 23456, 10.01m, true, true);
        Buyable testItemThree = new Buyable("Test Item Three", 45678, 199.99m, false, true);
        Buyable testItemFour = new Buyable("Test Item Four", 56789, 2999.99m, false, false);
        Buyable testItemDuplicate = new Buyable("Test Item Duplicate", 12345, 111.00m, true, false);

        [TestMethod]
        public void TestAdd()
        {
            Buyable expectedOne = Catalog.Instance.AddProduct("Test Item One", 12345, 14.00m, true, false);
            Buyable expectedTwo = Catalog.Instance.AddProduct("Test Item Two", 12345, 14.00m, true, false);

            Assert.AreEqual(expectedOne, testItemOne);
            Assert.AreEqual(expectedTwo, testItemOne); // This item's SKU already exists, so we'll get product one back
        }

        [TestMethod]
        public void TestGet()
        {
            Buyable expectedOne;
            Buyable expectedTwo;

            Catalog.Instance.GetProductBySKU(12345, out expectedOne);
            Catalog.Instance.GetProductBySKU(75757, out expectedTwo); // No item 75757

            Assert.AreEqual(expectedOne, testItemOne);
            Assert.AreEqual(null, expectedTwo);
        }
    }

    [TestClass]
    public class TestCartItem
    {
        Buyable testItemOne = new Buyable("Test Item One", 12345, 14.00m, true, false);
        Buyable testItemTwo = new Buyable("Test Item Two", 23456, 10.01m, true, true);
        Buyable testItemThree = new Buyable("Test Item Three", 56789, 2999.99m, false, false);

        [TestMethod]
        public void TestToString()
        {
            CartItem testCartItemOne = new CartItem(testItemOne, 3);
            CartItem testCartItemTwo = new CartItem(testItemTwo, 55);

            string expectedOne = "(3) Test Item One: 42.00";
            string expectedTwo = "(55) Imported Test Item Two: 578.10";

            Assert.AreEqual(expectedOne, testCartItemOne.ToString());
            Assert.AreEqual(expectedTwo, testCartItemTwo.ToString());
        }

        [TestMethod]
        public void TestSalesTaxes()
        {
            CartItem testCartItemOne = new CartItem(testItemOne, 3);
            CartItem testCartItemTwo = new CartItem(testItemTwo, 55);
            CartItem testCartItemThree = new CartItem(testItemThree, 173);

            Assert.AreEqual(0.00m, testCartItemOne.SalesTaxPerQty());
            Assert.AreEqual(0.00m, testCartItemTwo.SalesTaxPerQty());
            Assert.AreEqual(299.9990m, testCartItemThree.SalesTaxPerQty()); // Not rounded since it's not the final calculation
            Assert.AreEqual(51899.85m, testCartItemThree.TotalSalesTax()); // Rounded since it's the final calculation
        }

        [TestMethod]
        public void TestImportDuty()
        {
            CartItem testCartItemOne = new CartItem(testItemOne, 3);
            CartItem testCartItemTwo = new CartItem(testItemTwo, 55);
            CartItem testCartItemThree = new CartItem(testItemThree, 173);

            Assert.AreEqual(0.00m, testCartItemOne.ImportDutyPerQty());
            Assert.AreEqual(0.5005m, testCartItemTwo.ImportDutyPerQty()); // Not rounded since it's not the final calculation
            Assert.AreEqual(27.55m, testCartItemTwo.TotalImportDuty()); // Rounded since it's the final calculation
            Assert.AreEqual(0.00m, testCartItemThree.ImportDutyPerQty());
        }

        [TestMethod]
        public void TestCost()
        {
            CartItem testCartItemOne = new CartItem(testItemOne, 3);
            CartItem testCartItemTwo = new CartItem(testItemTwo, 55);
            CartItem testCartItemThree = new CartItem(testItemThree, 173);

            Assert.AreEqual(42.00m, testCartItemOne.TotalCost());
            Assert.AreEqual(578.10m, testCartItemTwo.TotalCost());
            Assert.AreEqual(570898.12m, testCartItemThree.TotalCost());
        }
    }

    [TestClass]
    public class TestShoppingCart
    {
        Buyable testItemOne = new Buyable("Test Item One", 12345, 14.00m, true, false);
        Buyable testItemTwo = new Buyable("Test Item Two", 23456, 10.01m, true, true);
        Buyable testItemThree = new Buyable("Test Item Three", 56789, 2999.99m, false, false);
        Buyable testItemDuplicate = new Buyable("Test Item Duplicate", 12345, 111.00m, true, false);

        [TestMethod]
        public void TestAddRemoveItem()
        {
            ShoppingCart testCart = new ShoppingCart();

            testCart.AddItem(testItemOne, 3);
            testCart.AddItem(testItemThree, 5);

            Assert.AreEqual(16541.95m, testCart.TotalPrice);

            testCart.AddItem(testItemTwo, 2);

            Assert.AreEqual(16563.02m, testCart.TotalPrice);

            testCart.RemoveItem(testItemTwo, 2);

            Assert.AreEqual(16541.95m, testCart.TotalPrice);

            testCart.RemoveItem(testItemOne, 3);

            Assert.AreEqual(16499.95m, testCart.TotalPrice);

            testCart.RemoveItem(testItemThree, 3);

            Assert.AreEqual(6599.98m, testCart.TotalPrice);

            testCart.RemoveItem(testItemThree, 3); // Only 2 should be left, but over-removing...

            Assert.AreEqual(0.00m, testCart.TotalPrice);
        }

        [TestMethod]
        public void TestTotalTaxes()
        {
            ShoppingCart testCart = new ShoppingCart();

            testCart.AddItem(testItemOne, 3);
            testCart.AddItem(testItemThree, 5);

            Assert.AreEqual(1500.00m, testCart.TotalTaxes());

            testCart.AddItem(testItemTwo, 2);

            Assert.AreEqual(1501.05m, testCart.TotalTaxes());

            testCart.RemoveItem(testItemTwo, 2);

            Assert.AreEqual(1500.00m, testCart.TotalTaxes());

            testCart.RemoveItem(testItemOne, 3);

            Assert.AreEqual(1500.00m, testCart.TotalTaxes());

            testCart.RemoveItem(testItemThree, 3);

            Assert.AreEqual(600.00m, testCart.TotalTaxes());

            testCart.RemoveItem(testItemThree, 3); // Only 2 should be left, but over-removing...

            Assert.AreEqual(0.00m, testCart.TotalTaxes());
        }
    }
}
