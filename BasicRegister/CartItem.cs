﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BasicRegister
{
    public class CartItem : INotifyPropertyChanged
    {
        private int _quantity;

        /// <summary>
        /// Quantity of a given product in the shopping cart
        /// </summary>
        public int Quantity
        {
            get { return _quantity; }
            private set
            {
                if (_quantity == value) return;
                _quantity = value;
                NotifyPropertyChanged("Quantity");
            }
        }

        /// <summary>
        /// Reference to the product in the shopping cart
        /// </summary>
        public Buyable Product { get; private set; }

        /// <summary>
        /// Property changed event handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Basic constructor for a cart item
        /// </summary>
        /// <param name="product">Product represented by the item</param>
        /// <param name="quantity">Quantity of product to initially set</param>
        public CartItem(Buyable product, int quantity = 1)
        {
            Product = product;
            Quantity = quantity;
        }

        /// <summary>
        /// Add the given quantity to the cart item
        /// </summary>
        /// <param name="quantityToAdd">Quantity to add to the item</param>
        public void AddQuantity(int quantityToAdd)
        {
            if (quantityToAdd <= 0) return;

            Quantity += quantityToAdd;
        }

        /// <summary>
        /// Remove the given quantity from the cart item
        /// </summary>
        /// <param name="quantityToRemove">Quantity to remove from the item</param>
        public void RemoveQuantity(int quantityToRemove)
        {
            if ( quantityToRemove <= 0) return;

            Quantity = quantityToRemove >= Quantity ? 0 : Quantity - quantityToRemove;
        }

        /// <summary>
        /// Total cost of total quantity of the cart item, including sales and import taxes
        /// </summary>
        /// <returns>Total cost of the cart item</returns>
        public decimal TotalCost()
        {
            return (Product.Price * Quantity) + TotalImportDuty() + TotalSalesTax();
        }

        /// <summary>
        /// Generic comparison operator between CartItem and generic object
        /// </summary>
        /// <param name="obj">Object to compare</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            CartItem objAsCartItem = obj as CartItem;
            if (objAsCartItem == null) return false;
            else return Equals(objAsCartItem);
        }

        /// <summary>
        /// Basic comparison operator between two CartItem objects
        /// </summary>
        /// <param name="other">CartItem to compare</param>
        /// <returns></returns>
        public bool Equals(CartItem other)
        {
            if (other == null) return false;
            return (Product.Equals(other.Product));
        }

        /// <summary>
        /// Reports a string representation of the product in the cart and its total cost with taxes
        /// </summary>
        /// <returns>String in the format of "(Quantity) Product: x.xx"</returns>
        public override string ToString()
        {
            string display = Product.ToString() + ": " + TotalCost().ToString("#,##0.00");

            display = "(" + Quantity + ")" + " " + display;

            return display;
        }

        /// <summary>
        /// Override of the Hash code function returning the associated product SKU
        /// </summary>
        /// <returns>Product number/SKU</returns>
        public override int GetHashCode()
        {
            return Product.GetHashCode();
        }

        /// <summary>
        /// Calculates the sales tax owed for an item per quantity, unrounded
        /// </summary>
        /// <returns>Sales tax per quantity in the</returns>
        public decimal SalesTaxPerQty()
        {
            if (!Product.IsTaxExempt) return Product.Price * PointOfSale.SALES_TAX_RATE;

            return 0.00m;
        }

        /// <summary>
        /// Calculates the import duty tax owed for an item per quantity, unrounded
        /// </summary>
        /// <returns>Import duty per quantity in the cart</returns>
        public decimal ImportDutyPerQty()
        {
            if (Product.IsImport) return Product.Price * PointOfSale.IMPORT_DUTY_RATE;

            return 0.00m;
        }

        /// <summary>
        /// Calculates the sales tax owed for the total quantity, rounded up to the nearest .05
        /// </summary>
        /// <returns>Total rounded sales tax owed for the product</returns>
        public decimal TotalSalesTax()
        {
            return Math.Ceiling(SalesTaxPerQty() * Quantity * 20) / 20; 
        }

        /// <summary>
        /// Calculates the import duty tax owed for the total quantity, rounded up to the nearest .05
        /// </summary>
        /// <returns>Total rounded import duty tax owed for the product</returns>
        public decimal TotalImportDuty()
        {
            return Math.Ceiling(ImportDutyPerQty() * Quantity * 20) / 20;
        }

        /// <summary>
        /// NotifyPropertyChanged handler
        /// </summary>
        /// <param name="propertyName">Name of the property being modified</param>
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
