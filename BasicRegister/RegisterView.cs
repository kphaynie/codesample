﻿using System;
using System.Windows.Forms;

namespace BasicRegister
{
    public partial class RegisterView : Form
    {
        private ShoppingCart _shoppingCart;

        public RegisterView()
        {
            InitializeComponent();
        }

        private void RegisterView_Load(object sender, EventArgs e)
        {
            _shoppingCart = new ShoppingCart();
        }

        private void btnBasketOne_Click(object sender, System.EventArgs e)
        {
            _shoppingCart.EmptyCart();
            _shoppingCart.AddItemBySku(11111);
            _shoppingCart.AddItemBySku(22222);
            _shoppingCart.AddItemBySku(33333);
            UpdateShoppingCart();
        }

        private void btnBasketTwo_Click(object sender, System.EventArgs e)
        {
            _shoppingCart.EmptyCart();
            _shoppingCart.AddItemBySku(44444);
            _shoppingCart.AddItemBySku(55555);
            UpdateShoppingCart();
        }

        private void btnBasketThree_Click(object sender, System.EventArgs e)
        {
            _shoppingCart.EmptyCart();
            _shoppingCart.AddItemBySku(66666);
            _shoppingCart.AddItemBySku(77777);
            _shoppingCart.AddItemBySku(88888);
            _shoppingCart.AddItemBySku(99999);
            UpdateShoppingCart();
        }

        private void UpdateShoppingCart()
        {
            tbShoppingCart.Text = _shoppingCart.ToString();
        }
    }
}
