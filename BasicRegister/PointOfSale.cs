﻿using System;
using System.Windows.Forms;



namespace BasicRegister
{
    static class PointOfSale
    {
        /// <summary>
        /// Constants containing the base Sales Tax and Import Duty rates
        /// </summary>
        public const decimal SALES_TAX_RATE = 0.10m;
        public const decimal IMPORT_DUTY_RATE = 0.05m;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new RegisterView());
            //var productCatalog = Catalog.Instance;

            //ShoppingCart testCartOne = new ShoppingCart();
            //ShoppingCart testCartTwo = new ShoppingCart();
            //ShoppingCart testCartThree = new ShoppingCart();

            //testCartOne.AddItemBySku(11111); // 16lb bag of Skittles
            //testCartOne.AddItemBySku(22222); // Walkman
            //testCartOne.AddItemBySku(33333); // bag of microwave Popcorn

            //Console.WriteLine(testCartOne.ToString());

            //testCartTwo.AddItemBySku(44444); // bag of Vanilla-Hazelnut Coffee
            //testCartTwo.AddItemBySku(55555); // Vespa

            //Console.WriteLine(testCartTwo.ToString());

            //testCartThree.AddItemBySku(66666); // crate of Almond Snickers
            //testCartThree.AddItemBySku(77777); // Discman
            //testCartThree.AddItemBySku(88888); // Bottle of Wine
            //testCartThree.AddItemBySku(99999); // 300# bag of Fair-Trade Coffee

            //Console.WriteLine(testCartThree.ToString());
        }
    }
}
