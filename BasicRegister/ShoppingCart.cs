﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace BasicRegister
{
    public class ShoppingCart
    {
        private readonly ObservableCollection<CartItem> _itemList; // Backing data structure for the Shopping Cart
        private bool _isDirty; // Flag set when an item in the Shopping Cart changes and taxes need to be recalculated
        private decimal _totalPrice;
        private decimal _salesTax;
        private decimal _importDuties;

        /// <summary>
        /// Total Sales Taxes for items in the cart
        /// </summary>
        public decimal SalesTax
        {
            get
            {
                CalculateCosts();
                return _salesTax;
            }
            private set { _salesTax = value; }
        }

        /// <summary>
        /// Total Import Duties for items in the cart
        /// </summary>
        public decimal ImportDuties
        {
            get
            {
                CalculateCosts();
                return _importDuties;
            }
            private set { _importDuties = value; }
        }

        /// <summary>
        /// Total Price for all items in the cart
        /// </summary>
        public decimal TotalPrice
        {
            get
            {
                CalculateCosts();
                return _totalPrice;
            }
            private set { _totalPrice = value; }
        }

        /** In a real implementation, this could probably be implemented as a singleton, but I wanted to maintain the ability to create
         * multiple carts for the time being **/
        public ShoppingCart()
        {
            _itemList = new ObservableCollection<CartItem>();
            _itemList.CollectionChanged += cartChanged;
        }
        
        /// <summary>
        /// Add a Product to the shopping cart with the given quantity. Adds the quantity if the item already exists.
        /// </summary>
        /// <param name="productToAdd">Product you wish to add to the shopping cart</param>
        /// <param name="quantity">Quantity of the product you wish to add</param>
        public void AddItem(Buyable productToAdd, int quantity = 1)
        {           
            if (!_itemList.Any(item => item.Product.SKU == productToAdd.SKU))
            {
                CartItem newItem = new CartItem(productToAdd, quantity);
                _itemList.Add(newItem);
            } else
            {
                _itemList.Single(item => item.Product.SKU == productToAdd.SKU).AddQuantity(quantity);
            }
        }

        /// <summary>
        /// Add a Product to the shopping cart by SKU with the given quantity. Adds the quantity if the item already exists.
        /// </summary>
        /// <param name="productToAdd">SKU of the Product you wish to add to the shopping cart</param>
        /// <param name="quantity">Quantity of the product you wish to add</param>
        public void AddItemBySku(int skuToAdd, int quantity = 1)
        {
            Buyable productToAdd;

            if ( Catalog.Instance.GetProductBySKU(skuToAdd, out productToAdd) )
            {
                AddItem(productToAdd, quantity);
            } else
            {
                // This should probably be an exception or error if the product does not exist in the Catalog
            }
        }

        /// <summary>
        /// Remove a quantity of a product from the shopping cart.
        /// </summary>
        /// <param name="productToRemove">Product you wish to remove quantity from</param>
        /// <param name="quantity">Quantity of the product you wish to remove</param>
        public void RemoveItem(Buyable productToRemove, int quantity = 1 )
        {
            RemoveItemBySku(productToRemove.SKU, quantity);
        }

        /// <summary>
        /// Add a Product to the shopping cart by SKU with the given quantity. Adds the quantity if the item already exists.
        /// </summary>
        /// <param name="productToAdd">SKU of the Product you wish to add to the shopping cart</param>
        /// <param name="quantity">Quantity of the product you wish to add</param>
        public void RemoveItemBySku(int skuToRemove, int quantity = 1)
        {
            if (!_itemList.Any(item => item.Product.SKU == skuToRemove)) return;

            CartItem itemToRemove = _itemList.Single(item => item.Product.SKU == skuToRemove);

            if ( itemToRemove.Quantity <= quantity )
            {
                _itemList.Remove(itemToRemove);
            } else
            {
                itemToRemove.RemoveQuantity(quantity);
            }
        }

        /// <summary>
        /// Find a product in the shopping cart by SKU. Returns a reference to the object, if it exists.
        /// </summary>
        /// <param name="skuToFind">SKU of the Product you wish to find in the shopping cart</param>
        /// <returns></returns>
        public CartItem GetItemBySku(int skuToFind)
        {
            if (!_itemList.Any(item => item.Product.SKU == skuToFind)) return null;

            return _itemList.Single(item => item.Product.SKU == skuToFind);
        }

        /// <summary>
        /// Event raised when the Cart changes (i.e. a product is added or removed)
        /// Side effect: Sets dirty flag
        /// </summary>
        /// <param name="sender">Object raising the event</param>
        /// <param name="args">EventChanged args, containing list of objects added/removed</param>
        private void cartChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            if (args.NewItems != null)
            {
                foreach (Object item in args.NewItems)
                {
                    ((INotifyPropertyChanged)item).PropertyChanged += cartItemChanged;
                }
            }

            if (args.OldItems != null)
            {
                foreach (Object item in args.OldItems)
                {
                    ((INotifyPropertyChanged)item).PropertyChanged -= cartItemChanged;
                }
            }

            _isDirty = true;
        }

        /// <summary>
        /// Event raised when a CartItem in the cart is modified (i.e. quantity changes)
        /// Side effect: Sets dirty flag
        /// </summary>
        /// <param name="sender">Object raising the event</param>
        /// <param name="args">PropertyChanged args, containing list of properties changed</param>
        private void cartItemChanged(object sender, PropertyChangedEventArgs args)
        {
            _isDirty = true;
        }

        /// <summary>
        /// Removes all items in the cart. Who would want to do this?
        /// </summary>
        public void EmptyCart()
        {
            _itemList.Clear();
        }

        /// <summary>
        /// If needed, calculates the total SalesTax, ImportDuty, and Price for all items in the cart
        /// Side effect: Clears dirty flag
        /// </summary>
        private void CalculateCosts()
        {
            if (!_isDirty) return;

            _isDirty = false;

            SalesTax = 0.00m;
            ImportDuties = 0.00m;
            TotalPrice = 0.00m;

            foreach (CartItem item in _itemList)
            {
                SalesTax += item.TotalSalesTax();
                ImportDuties += item.TotalImportDuty();
                TotalPrice += item.TotalCost();
            }
        }

        /// <summary>
        /// Reports a string representation of the contents of the cart and its taxes and cost
        /// </summary>
        /// <returns>String containing all products in the cart, all sales taxes, and the total cost</returns>
        public override string ToString()
        {
            string output = "";


            foreach(CartItem item in _itemList)
            {
                output += item.ToString() + "\r\n";
            }

            if (_itemList.Count == 0) output += "Your cart is empty.\r\n";

            output += "Sales Taxes: " + TotalTaxes().ToString("#,##0.00") + "\r\n";
            output += "Total: " + TotalPrice.ToString("#,##0.00") + "\r\n";

            return output;
        }

        /// <summary>
        /// Forces calculation of taxes and calculates the sum of Sales and Import Taxes for the cart
        /// </summary>
        /// <returns>Sum total of all Sales and Import taxes for items in the cart</returns>
        public decimal TotalTaxes()
        {
            return SalesTax + ImportDuties;
        }
    }
}
