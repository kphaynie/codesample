﻿namespace BasicRegister
{
    /// <summary>
    /// Object representing an item that can be purchased, whether in inventory or referenced from a shopping cart
    /// </summary>
    public class Buyable
    {
        /// <summary>
        /// Name of the product
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Tax exempt status of the product
        /// </summary>
        public bool IsTaxExempt { get; private set; }

        /// <summary>
        /// Import status of the product
        /// </summary>
        public bool IsImport { get; private set; }

        /// <summary>
        /// Base price of the product before taxes
        /// </summary>
        public decimal Price { get; private set; }

        /// <summary>
        /// Unique product number/SKU
        /// </summary>
        public int SKU { get; private set; }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="name">Name of the product</param>
        /// <param name="sku">Unique product number/SKU</param>
        /// <param name="price">Base price of the product before taxes</param>
        /// <param name="taxExempt">Tax exempt status of the product</param>
        /// <param name="import">Import status of the product</param>
        public Buyable(string name, int sku, decimal price, bool taxExempt, bool import)
        {
            Name = name;
            Price = price;
            IsTaxExempt = taxExempt;
            IsImport = import;
            SKU = sku;
        }

        /// <summary>
        /// Basic comparison operator between two Buyable objects
        /// </summary>
        /// <param name="other">Buyable to compare</param>
        /// <returns></returns>
        public bool Equals(Buyable other)
        {
            if (other == null) return false;
            return (SKU.Equals(other.SKU));
        }

        /// <summary>
        /// Generic comparison operator between Buyable and generic object
        /// </summary>
        /// <param name="obj">Object to compare</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            Buyable objAsBuyable = obj as Buyable;
            if (objAsBuyable == null) return false;
            else return Equals(objAsBuyable);
        }

        /// <summary>
        /// Reports a string representation of the product, including whether or not it is an import
        /// </summary>
        /// <returns>String in the format of "[Imported] Name"</returns>
        public override string ToString()
        {
            string display = Name;

            if (IsImport)
            {
                display = "Imported " + display;
            }

            return display;
        }

        /// <summary>
        /// Override of the Hash code function returning the product SKU
        /// </summary>
        /// <returns>Product number/SKU</returns>
        public override int GetHashCode()
        {
            return SKU;
        }
    }
}
