﻿namespace BasicRegister
{
    partial class RegisterView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPresets = new System.Windows.Forms.Label();
            this.btnBasketOne = new System.Windows.Forms.Button();
            this.btnBasketTwo = new System.Windows.Forms.Button();
            this.btnBasketThree = new System.Windows.Forms.Button();
            this.lblShoppingCart = new System.Windows.Forms.Label();
            this.tbShoppingCart = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblPresets
            // 
            this.lblPresets.AutoSize = true;
            this.lblPresets.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPresets.Location = new System.Drawing.Point(12, 374);
            this.lblPresets.Name = "lblPresets";
            this.lblPresets.Size = new System.Drawing.Size(149, 13);
            this.lblPresets.TabIndex = 0;
            this.lblPresets.Text = "Preset Shopping Baskets";
            // 
            // btnBasketOne
            // 
            this.btnBasketOne.Location = new System.Drawing.Point(12, 400);
            this.btnBasketOne.Name = "btnBasketOne";
            this.btnBasketOne.Size = new System.Drawing.Size(92, 38);
            this.btnBasketOne.TabIndex = 2;
            this.btnBasketOne.Text = "Shopping Basket 1";
            this.btnBasketOne.UseVisualStyleBackColor = true;
            this.btnBasketOne.Click += new System.EventHandler(this.btnBasketOne_Click);
            // 
            // btnBasketTwo
            // 
            this.btnBasketTwo.Location = new System.Drawing.Point(110, 400);
            this.btnBasketTwo.Name = "btnBasketTwo";
            this.btnBasketTwo.Size = new System.Drawing.Size(92, 38);
            this.btnBasketTwo.TabIndex = 3;
            this.btnBasketTwo.Text = "Shopping Basket 2";
            this.btnBasketTwo.UseVisualStyleBackColor = true;
            this.btnBasketTwo.Click += new System.EventHandler(this.btnBasketTwo_Click);
            // 
            // btnBasketThree
            // 
            this.btnBasketThree.Location = new System.Drawing.Point(208, 400);
            this.btnBasketThree.Name = "btnBasketThree";
            this.btnBasketThree.Size = new System.Drawing.Size(92, 38);
            this.btnBasketThree.TabIndex = 4;
            this.btnBasketThree.Text = "Shopping Basket 3";
            this.btnBasketThree.UseVisualStyleBackColor = true;
            this.btnBasketThree.Click += new System.EventHandler(this.btnBasketThree_Click);
            // 
            // lblShoppingCart
            // 
            this.lblShoppingCart.AutoSize = true;
            this.lblShoppingCart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShoppingCart.Location = new System.Drawing.Point(12, 9);
            this.lblShoppingCart.Name = "lblShoppingCart";
            this.lblShoppingCart.Size = new System.Drawing.Size(87, 13);
            this.lblShoppingCart.TabIndex = 5;
            this.lblShoppingCart.Text = "Shopping Cart";
            // 
            // tbShoppingCart
            // 
            this.tbShoppingCart.Location = new System.Drawing.Point(15, 38);
            this.tbShoppingCart.Multiline = true;
            this.tbShoppingCart.Name = "tbShoppingCart";
            this.tbShoppingCart.Size = new System.Drawing.Size(285, 323);
            this.tbShoppingCart.TabIndex = 6;
            this.tbShoppingCart.ReadOnly = true;
            // 
            // RegisterView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 447);
            this.Controls.Add(this.tbShoppingCart);
            this.Controls.Add(this.lblShoppingCart);
            this.Controls.Add(this.btnBasketThree);
            this.Controls.Add(this.btnBasketTwo);
            this.Controls.Add(this.btnBasketOne);
            this.Controls.Add(this.lblPresets);
            this.Name = "RegisterView";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.RegisterView_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPresets;
        private System.Windows.Forms.Button btnBasketOne;
        private System.Windows.Forms.Button btnBasketTwo;
        private System.Windows.Forms.Button btnBasketThree;
        private System.Windows.Forms.Label lblShoppingCart;
        private System.Windows.Forms.TextBox tbShoppingCart;
    }
}

