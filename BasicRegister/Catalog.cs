﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BasicRegister
{
    public sealed class Catalog
    {
        /// <summary>
        /// Backing data structure of the catalog products, keyed by SKU
        /// </summary>
        private Dictionary<int, Buyable> _productList = new Dictionary<int, Buyable>();

        /// <summary>
        /// Lazy implementation of singleton instance of catalog
        /// </summary>
        private static Lazy<Catalog> _instance = new Lazy<Catalog>(() => new Catalog());

        /// <summary>
        /// Catalog instance property
        /// </summary>
        public static Catalog Instance { get { return _instance.Value; } }

        /// <summary>
        /// Basic constructor for Catalog. Contains hardcoded initial product list.
        /// </summary>
        private Catalog()
        {
            // I would normally have implemented something more robust here, i.e. a JSON deserializer w/ a JSON "database", a real database, etc... but this is sufficient for now.
            AddProduct("16lb bag of Skittles", 11111, 16.00m, true, false);
            AddProduct("Walkman", 22222, 99.99m, false, false);
            AddProduct("bag of microwave Popcorn", 33333, 0.99m, true, false);
            AddProduct("bag of Vanilla-Hazelnut Coffee", 44444, 11.00m, true, true);
            AddProduct("Vespa", 55555, 15001.25m, false, true);
            AddProduct("crate of Almond Snickers", 66666, 75.99m, true, true);
            AddProduct("Discman", 77777, 55.00m, false, false);
            AddProduct("Bottle of Wine", 88888, 10.00m, false, true);
            AddProduct("300# bag of Fair-Trade Coffee", 99999, 997.99m, true, false);
        }

        /// <summary>
        /// Add a Buyable product to the catalog if it does not already exist, otherwise return the item matching the SKU provided.
        /// </summary>
        /// <param name="name">Name of the product</param>
        /// <param name="sku">Unique product number/SKU</param>
        /// <param name="price">Base price of the product before taxes</param>
        /// <param name="taxExempt">Tax exempt status of the product</param>
        /// <param name="import">Import status of the product</param>
        /// <returns>Reference to the Buyable object in the catalog</returns>
        public Buyable AddProduct(string name, int sku, decimal price, bool taxExempt, bool import)
        {
            Buyable productToAdd;

            if (!_productList.Any(item => item.Value.SKU == sku))
            {
                productToAdd = new Buyable(name, sku, price, taxExempt, import);

                _productList.Add(sku, productToAdd);
            } else
            {
                _productList.TryGetValue(sku, out productToAdd);
                /* I should probably simply disallow this or throw an exception, but leaving this implementation for simplicity.
                 * Receiving a different product when supplying the same SKU is probably less than ideal. */
            }

            return productToAdd;
        }

        /// <summary>
        /// Retrieve the product in the Catalog with the provided SKU
        /// </summary>
        /// <param name="sku">Unique product number/SKU to retrieve</param>
        /// <param name="product">Output variable containing the Buyable product in the Catalog</param>
        /// <returns></returns>
        public bool GetProductBySKU(int sku, out Buyable product)
        {
            return _productList.TryGetValue(sku, out product);
        }

        /// <summary>
        /// Basic function to print the contents of the catalog. No practical use, just used for debugging.
        /// </summary>
        public void PrintCatalog()
        {
            Console.WriteLine("Full Catalog:");

            foreach(KeyValuePair<int,Buyable> item in _productList) 
            {
                Console.WriteLine("SKU: " + item.Key + " - " + item.Value.ToString() + " - " + item.Value.Price.ToString("#,##0.00"));
            }
        }
    }
}
